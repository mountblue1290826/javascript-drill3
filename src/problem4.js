export function getGroupsByLanguage(users) {
  if (typeof users !== "object" || users === null) {
    throw new Error("Input must be a non-null object");
  }

  const groups = {};
  for (const user in users) {
    const desgination = users[user].desgination;
    if (desgination.indexOf("Golang") !== -1) {
      if (groups.golang === undefined) {
        groups["golang"] = [];
        groups.golang.push(user);
      } else {
        groups.golang.push(user);
      }
    }

    if (desgination.indexOf("Javascript") !== -1) {
      if (groups.javascript === undefined) {
        groups["javascript"] = [];
        groups.javascript.push(user);
      } else {
        groups.javascript.push(user);
      }
    }

    if (desgination.indexOf("Python") !== -1) {
      if (groups.python === undefined) {
        groups["python"] = [];
        groups.python.push(user);
      } else {
        groups.python.push(user);
      }
    }
  }
  return groups;
}
