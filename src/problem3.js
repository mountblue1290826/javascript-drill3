export function getUsersByQualification(users, qualification) {
  if (typeof users !== "object" || users === null) {
    throw new Error("First parameter must be a non-null object");
  }
  if (typeof qualification !== "string") {
    throw new Error("Second parameter must be string");
  }
  let res = [];
  for (const user in users) {
    if (users[user].qualification === qualification) {
      res.push(user);
    }
  }
  return res;
}
