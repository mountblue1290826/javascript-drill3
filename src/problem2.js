export function getUsersByLocation(users, location) {
  if (typeof users !== "object" || users === null) {
    throw new Error("First parameter must be a non-null object");
  }
  if (typeof location !== "string") {
    throw new Error("Second parameter must be string");
  }
  let res = [];
  for (const user in users) {
    if (users[user].nationality === location) {
      res.push(user);
    }
  }
  return res;
}
