export function getAllUsersPlayingVideoGame(users) {
  if (typeof users !== "object" || users === null) {
    throw new Error("Input must be a non-null object");
  }
  const res = [];
  for (const user in users) {
    const interests = users[user].interests[0].split(", ");
    for (let i = 0; i < interests.length; i++) {
      if (interests[i].indexOf("Video Games") !== -1) {
        res.push(user);
      }
    }
  }
  return res;
}
