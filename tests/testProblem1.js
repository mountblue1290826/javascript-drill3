import { users } from "../users.js";
import { getAllUsersPlayingVideoGame } from "../src/problem1.js";

let usersPlayingVideoGame = "";

try {
  usersPlayingVideoGame = getAllUsersPlayingVideoGame("users");
  console.log(usersPlayingVideoGame);
} catch (error) {
  console.log(error.message);
}

try {
  usersPlayingVideoGame = getAllUsersPlayingVideoGame(users);
  console.log(usersPlayingVideoGame);
} catch (error) {
  console.log(error.message);
}
