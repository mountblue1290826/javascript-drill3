import { users } from "../users.js";
import { getUsersByQualification } from "../src/problem3.js";

let usderByQualifications;

try {
  usderByQualifications = getUsersByQualification(null, "Masters");
  console.log(usderByQualifications);
} catch (error) {
  console.log(error.message);
}

try {
  usderByQualifications = getUsersByQualification(users, []);
  console.log(usderByQualifications);
} catch (error) {
  console.log(error.message);
}

try {
  usderByQualifications = getUsersByQualification(users, "Masters");
  console.log(usderByQualifications);
} catch (error) {
  console.log(error.message);
}
