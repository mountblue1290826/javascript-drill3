import { users } from "../users.js";
import { getUsersByLocation } from "../src/problem2.js";

let userByLocation;

try {
  userByLocation = getUsersByLocation(null, "Germany");
  console.log(userByLocation);
} catch (error) {
  console.log(error.message);
}

try {
  userByLocation = getUsersByLocation(users, 10);
  console.log(userByLocation);
} catch (error) {
  console.log(error.message);
}

try {
  userByLocation = getUsersByLocation(users, "Germany");
  console.log(userByLocation);
} catch (error) {
  console.log(error.message);
}
