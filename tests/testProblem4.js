import { users } from "../users.js";
import { getGroupsByLanguage } from "../src/problem4.js";

let groupsByLanguage;

try {
  groupsByLanguage = getGroupsByLanguage("users");
  console.log(groupsByLanguage);
} catch (error) {
  console.log(error.message);
}

try {
  groupsByLanguage = getGroupsByLanguage(users);
  console.log(groupsByLanguage);
} catch (error) {
  console.log(error.message);
}
